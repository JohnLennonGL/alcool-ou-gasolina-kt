package com.jlngls.alcoolougasolinakt

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
        fun CalcularPreco(view: View) {


            var precoAlcool = alcoolID.text.toString()
            var precoGasolina = gasolinaID.text.toString()


            var validaCampos = validarCampos(precoAlcool, precoGasolina)

            if (validaCampos) {
                CalcularMelhorPreco(precoAlcool, precoGasolina)
            } else {
                resultadoID.text = "verifique se algum campo estar vazio"
            }

        }


    fun CalcularMelhorPreco(valorAlcool: String , valorGasolina : String){


        val precoAlcool = valorAlcool.toDouble()
        val precoGasolina = valorGasolina.toDouble()


        val resultado = precoAlcool / precoGasolina
        if(resultado >= 0.7){
            resultadoID.text = "É melhor usar gasolina"
        }
        else{
            resultadoID.setText("É melhor usar alcool")
        }



}

    fun validarCampos(alcool: String , gasolina : String): Boolean{

        var camposValidados = true

        if(alcool == null || alcool.equals("") ){
            camposValidados = false
        }
        else if( gasolina == null || gasolina.equals("")){
            camposValidados = false
        }
         return camposValidados
    }

}